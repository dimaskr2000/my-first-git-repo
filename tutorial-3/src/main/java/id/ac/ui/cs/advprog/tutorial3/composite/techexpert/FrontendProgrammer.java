package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {
    //TODO Implement
    protected static final double MIN_SALARY = 30000.00;

    public FrontendProgrammer(String name, double salary) {
        try {
            this.name = name;
            this.salary = salary;
            this.role = "Front End Programmer";
            if (salary < MIN_SALARY) {
                throw new IllegalArgumentException(this.role + " Salary must not be lower than " + MIN_SALARY);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        }
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}