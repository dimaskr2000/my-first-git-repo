package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees {
    //TODO Implement
    protected static final double MIN_SALARY = 70000.00;
    
    public SecurityExpert(String name, double salary) {
        try {
            this.name = name;
            this.salary = salary;
            this.role = "Security Expert";
            if (salary < MIN_SALARY) {
                throw new IllegalArgumentException(this.role + " Salary must not be lower than " + MIN_SALARY);
            }
        } catch (IllegalArgumentException e) {
            throw e;
    }
}
    @Override
    public double getSalary() {
        return this.salary;
    }
}
