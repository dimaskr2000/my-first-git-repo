package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Main {
    public static void main(String[] args) {
        Food thickBunBurgerDemo = BreadProducer.THICK_BUN.createBreadToBeFilled();

        thickBunBurgerDemo = FillingDecorator.BEEF_MEAT.addFillingToBread(thickBunBurgerDemo);
        
        thickBunBurgerDemo = FillingDecorator.CHILI_SAUCE.addFillingToBread(thickBunBurgerDemo);

        thickBunBurgerDemo = FillingDecorator.CHEESE.addFillingToBread(thickBunBurgerDemo);

        thickBunBurgerDemo = FillingDecorator.CUCUMBER.addFillingToBread(thickBunBurgerDemo);

        thickBunBurgerDemo = FillingDecorator.LETTUCE.addFillingToBread(thickBunBurgerDemo);

        System.out.println("Pesanan anda adalah");
        System.out.println(thickBunBurgerDemo.getDescription());
        System.out.printf("Seharga $%.2f\n", thickBunBurgerDemo.cost());
    }
}
