package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    protected static final double MIN_SALARY = 100000.00;

    public Cto(String name, double salary) {
        try {
            this.name = name;
            this.salary = salary;
            this.role = "CTO";
            if (salary < MIN_SALARY) {
                throw new IllegalArgumentException(this.role + " Salary must not be lower than " + MIN_SALARY);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        }
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}