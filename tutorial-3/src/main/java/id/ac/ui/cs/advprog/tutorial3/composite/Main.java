package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class Main {
    public static void main(String[] args) {
        Company company = new Company();

        Employees naruto = new Ceo("CEO", 500000.00);
        company.addEmployee(naruto);

        Employees sasuke = new Cto("CTO", 320000.00);
        company.addEmployee(sasuke);

        Employees sakura = new BackendProgrammer("BACKEND", 94000.00);
        company.addEmployee(sakura);

        Employees hinata = new BackendProgrammer("BACKEND2", 200000.00);
        company.addEmployee(hinata);

        Employees kiba = new FrontendProgrammer("FRONTEND",66000.00);
        company.addEmployee(kiba);

        Employees shino = new FrontendProgrammer("FRONTEND2", 130000.00);
        company.addEmployee(shino);

        Employees neji = new UiUxDesigner("UIUX", 177000.00);
        company.addEmployee(neji);

        Employees kabuto = new NetworkExpert("NETRWORK", 83000.00);
        company.addEmployee(kabuto);

        Employees tobi = new SecurityExpert("SECURITY", 70000.00);
        company.addEmployee(tobi);

        System.out.println("List employee: " + company.getAllEmployees());
        System.out.println("Total gaji: " + company.getNetSalaries());
    }
}