package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class JavaneseClams implements Clams {

    public String toString() {
        return "Fresh Clams from Java Island";
    }
}
