package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TomatoSauceTest {
    private Sauce sauce;
    
    @Before
    public void setUp() {
        sauce = new TomatoSauce();
    }

    @Test
    public void testSauceName() {
        assertEquals("Plain Tomato Sauce", sauce.toString());
    }
}
