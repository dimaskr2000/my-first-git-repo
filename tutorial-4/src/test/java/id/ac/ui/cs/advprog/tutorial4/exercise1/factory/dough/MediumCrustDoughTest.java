package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MediumCrustDoughTest {
    private Dough dough;

    @Before
    public void setUp() {
        dough = new MediumCrustDough();
    }

    @Test
    public void testDoughName() {
        assertEquals("Medium Crust Dough", dough.toString());
    }
}
