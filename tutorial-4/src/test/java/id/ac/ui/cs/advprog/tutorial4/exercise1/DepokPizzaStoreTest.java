package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    private DepokPizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void testCreateCheesePizza() {
        Pizza pizza = depokPizzaStore.createPizza("cheese");
        assertEquals("Depok Style Cheese Pizza",pizza.getName());
    }

    @Test
    public void testCreateCheezyPizza() {
        Pizza pizza = depokPizzaStore.createPizza("veggie");
        assertEquals("Depok Style Veggie Pizza",pizza.getName());
    }

    @Test
    public void testCreateMixedPizza() {
        Pizza pizza = depokPizzaStore.createPizza("clam");
        assertEquals("Depok Style Clam Pizza",pizza.getName());
    }
}

