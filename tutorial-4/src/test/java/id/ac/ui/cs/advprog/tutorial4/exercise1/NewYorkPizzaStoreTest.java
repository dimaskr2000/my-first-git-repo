package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    private NewYorkPizzaStore newYorkPizzaStore;

    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testCreateCheesePizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("cheese");
        assertEquals("New York Style Cheese Pizza",pizza.getName());
    }

    @Test
    public void testCreateCheezyPizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("veggie");
        assertEquals("New York Style Veggie Pizza",pizza.getName());
    }

    @Test
    public void testCreateMixedPizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("clam");
        assertEquals("New York Style Clam Pizza",pizza.getName());
    }
}

