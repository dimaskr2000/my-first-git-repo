package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class JavaneseClamsTest {
    private Clams clam;

    @Before
    public void setUp() {
        clam = new JavaneseClams();
    }

    @Test
    public void testClamsName() {
        assertEquals("Fresh Clams from Java Island", clam.toString());
    }
}