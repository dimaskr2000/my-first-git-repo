package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaIngredientFactoryTest {

    private NewYorkPizzaIngredientFactory pizzaFactory;
    private Veggies[] veggiesNy;

    @Before
    public void setUp() {
        pizzaFactory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    public void testDoughCreated() {
        assertEquals("Thin Crust Dough", pizzaFactory.createDough().toString());
    }

    @Test
    public void testCheeseCreated() {
        assertEquals("Reggiano Cheese", pizzaFactory.createCheese().toString());
    }

    @Test
    public void testClamsCreated() {
        assertEquals("Fresh Clams from Long Island Sound", pizzaFactory.createClam().toString());
    }

    @Test
    public void testSauceCreated() {
        assertEquals("Marinara Sauce", pizzaFactory.createSauce().toString());
    }

    @Test
    public void testVeggiesCreated() {
        veggiesNy = pizzaFactory.createVeggies();
        assertEquals("Garlic", veggiesNy[0].toString());
        assertEquals("Onion", veggiesNy[1].toString());
        assertEquals("Mushrooms", veggiesNy[2].toString());
        assertEquals("Red Pepper", veggiesNy[3].toString());
    }
}
