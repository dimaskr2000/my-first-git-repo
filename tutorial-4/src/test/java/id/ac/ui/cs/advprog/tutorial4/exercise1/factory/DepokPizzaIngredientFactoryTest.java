package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaIngredientFactoryTest {

    private DepokPizzaIngredientFactory pizzaFactory;
    private Veggies[] veggiesDepok;

    @Before
    public void setUp() {
        pizzaFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testDoughCreated() {
        assertEquals("Medium Crust Dough", pizzaFactory.createDough().toString());
    }

    @Test
    public void testCheeseCreated() {
        assertEquals("Feta Goat Cheese", pizzaFactory.createCheese().toString());
    }

    @Test
    public void testClamsCreated() {
        assertEquals("Fresh Clams from Java Island", pizzaFactory.createClam().toString());
    }

    @Test
    public void testSauceCreated() {
        assertEquals("Plain Tomato Sauce", pizzaFactory.createSauce().toString());
    }

    @Test
    public void testVeggiesCreated() {
        veggiesDepok = pizzaFactory.createVeggies();
        assertEquals("Garlic", veggiesDepok[0].toString());
        assertEquals("Onion", veggiesDepok[1].toString());
        assertEquals("Spinach", veggiesDepok[2].toString());
        assertEquals("Jalapeno", veggiesDepok[3].toString());
    }
}
