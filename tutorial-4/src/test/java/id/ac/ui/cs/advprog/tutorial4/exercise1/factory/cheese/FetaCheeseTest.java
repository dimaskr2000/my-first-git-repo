package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FetaCheeseTest {
    private Cheese cheese;

    @Before
    public void setUp() {
        cheese = new FetaCheese();
    }

    @Test
    public void testCheeseName() {
        assertEquals("Feta Goat Cheese", cheese.toString());
    }
}
