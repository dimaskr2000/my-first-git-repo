package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FrozenClamsTest {
    private Clams clam;

    @Before
    public void setUp() {
        clam = new FrozenClams();
    }

    @Test
    public void testClamsName() {
        assertEquals("Frozen Clams from Chesapeake Bay", clam.toString());
    }
}
