package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    // TODO Complete me!
    public void setFlyBehavior(FlyBehavior f) {
        flyBehavior = f;
    }
    
    public void setQuackBehavior(QuackBehavior q) {
        quackBehavior = q;
    }

    public void swim() {
        System.out.println("Swimming");
    }

    abstract public void display();
}
