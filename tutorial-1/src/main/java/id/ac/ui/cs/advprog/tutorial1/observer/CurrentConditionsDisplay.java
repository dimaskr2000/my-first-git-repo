package id.ac.ui.cs.advprog.tutorial1.observer;

import com.sun.beans.WeakCache;
import java.util.Observable;
import java.util.Observer;

public class CurrentConditionsDisplay implements Observer, DisplayElement {

    private Observable observable;
    private float temperature;
    private float humidity;

    public CurrentConditionsDisplay(Observable observable) {
        // TODO Complete me!
        this.observable = observable;
        this.temperature = ((WeatherData) observable).getTemperature();
        this.humidity = ((WeatherData) observable).getHumidity();
        observable.addObserver(this);        
        
    }

    @Override
    public void display() {
        System.out.println("Current conditions: " + temperature
                + "F degrees and " + humidity + "% humidity");
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            // TODO Complete me!
            this.temperature = ((WeatherData) observable).getTemperature();
            this.humidity = ((WeatherData) observable).getHumidity();
            display();            
        }
    }
}
