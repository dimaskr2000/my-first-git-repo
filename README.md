### Name: DIMAS KRISSANTO RAHMADI<br>
### NPM: 1706979202<br>
### Class: B<br>
### Hobby: Music, Vidya & Fitness<br>


-------------Links-------------
- Practice Repo: https://gitlab.com/dimaskr2000/my-first-git-repo
- DDP2 Repo: https://gitlab.com/dimaskr2000/DDP2-Assignments

-----------My Notes------------
- Find a reason why you should use git branches!<br>
Branches dipakai jika ada lebih dari satu orang yang mengerjakan
sebuah project. Hal ini dilakukan agar setiap perubahan yang dibuat
oleh masing-masing orang tidak saling tumpang tindih,
dan konflik yang muncul bisa dicari solusinya satu persatu dengan aman.

- Describe how you use git branches on that repository!<br>
Pertama, sudah pasti terdapat branch master dalam sebuah repo. Bila kita
ingin membuat branch baru, ketikkan 'git branch (BRANCHNAME)'. Lalu,
untuk memakainya, gunakan 'git checkout (BRANCHNAME)'. Masing2 branch dapat melakukan
commit-commit, dan ketika dipush, perbedaan dalam setiap branch akan terlihat. Lalu lakukan
'git merge (BRANCHNAME)' dan konflik bisa di-ovveride secara manual dengan aman.

- Find a scenario so you should use git revert!<br>
Git revert dipakai ketika kita ingin me-rollback perubahan yang telah kita buat kepada file-file dalam repo.
Dengan kata lain, kita ingin mengembalikan kondisi project ke dalam kondisi sebuah commit yang pernah dilakukan
sebelumnya. Selain itu, kita pun juga dapat membuat perubahan-perubahan tambahan.

- Describe how you use git revert on that repository!<br>
Pertama, kita masukkan 'git log' untuk melihat commit-commit yang pernah dilakukan sebelumnya. Kemudian,
catatlah hashcode yang mereferensi ke kondisi commit yang ingin kita tuju untuk revert. Lalu, untuk memastikan
bahwa commit yang kita pilih sudah benar, kita dapat memakai 'git checkout (HASHCODE)' dan melakukan 'cat FILENAME'
untuk melihat kondisi file pada commit tersebut. Jika sudah benar, kembali ke branch master dengan 'git checkout master'
dan lakukan 'git revert (HASHCODE)'. Konflik seharusnya akan muncul dan kita dapat memeriksanya dengan 'git status'.
Kita dapat memodifikasi secara manual file-file yang konflik, lalu lakukan 'git add .' dan 'git revert --continue' untuk
melanjutkan, atau 'git revert --abort' untuk membatalkan. Ketikkan message revert pada vim, dan proses revert pun selesai.
